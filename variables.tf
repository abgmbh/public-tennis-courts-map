# Azure Environment
variable prefix { default = "cz-tf-t-app" }
variable location { default = "australiasoutheast" }
variable me {}

# NETWORK
variable cidr { default = "10.90.0.0/16" }
variable "subnets" {
  type = map
  default = {
    "ext" = "10.90.1.0/24"
  }
}
variable ext-gw { default = "10.90.1.1" }
variable ext-1-ip { default = "10.90.1.4" }

# VM
variable instance-type-1 { default = "Standard_F4s_v2" }
variable username { default = "ubuntu" }