# Azure Provider
provider "azurerm" {
  features {}
}

resource "random_id" "id" {
  byte_length = 2
}

# Create a Resource Group
resource "azurerm_resource_group" "main" {
  name     = format("%s-rg-%s", var.prefix, random_id.id.hex)
  location = var.location
}

# Create the Storage Account
resource "azurerm_storage_account" "mystorage" {
  name                     = format("storage%s", random_id.id.hex)
  resource_group_name      = azurerm_resource_group.main.name
  location                 = azurerm_resource_group.main.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}