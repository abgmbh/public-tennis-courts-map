resource "azurerm_public_ip" "ext-1-pip" {
  name                = "${var.prefix}-ext-1-pip"
  location            = azurerm_resource_group.main.location
  sku                 = "Standard"
  resource_group_name = azurerm_resource_group.main.name
  allocation_method   = "Static"
}