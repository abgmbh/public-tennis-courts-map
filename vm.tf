resource "azurerm_linux_virtual_machine" "NGINX-Web-Server-vm" {
  name                            = "${var.prefix}-NGINX-Web-Server-vm"
  location                        = azurerm_resource_group.main.location
  resource_group_name             = azurerm_resource_group.main.name
  network_interface_ids           = [azurerm_network_interface.ext-1-nic.id]
  size                            = var.instance-type-1
  admin_username                  = var.username
  disable_password_authentication = true
  computer_name                   = "${var.prefix}-NGINX-Web-Server-vm"

  os_disk {
    name                 = "${var.prefix}-NGINX-Web-Server-vm-osdisk"
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  admin_ssh_key {
    username   = var.username
    public_key = file("~/.ssh/id_rsa.pub")
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
}
